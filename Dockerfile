FROM golang:1.15.1-alpine as builder
COPY app/getip/main.go main.go
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags="-s -w" -o .tmp/getip main.go

FROM alpine:3.11.5
RUN apk add --no-cache ca-certificates tzdata
ENV TZ Asia/Jakarta
COPY --from=builder /go/.tmp/getip /usr/local/bin/getip

EXPOSE 7000
ENTRYPOINT ["getip"]