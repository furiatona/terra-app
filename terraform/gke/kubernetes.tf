terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
}

provider "kubernetes" {}

resource "kubernetes_deployment" "getip" {
  metadata {
    name = "getip-deployment"
    labels = {
      App = "getip"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "getip"
      }
    }
    template {
      metadata {
        labels = {
          App = "getip"
        }
      }
      spec {
        container {
          image = "furiatona/getip:0.1.0"
          name  = "getip"

          port {
            container_port = 7000
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "getip" {
  metadata {
    name = "getip-svc"
  }
  spec {
    selector = {
      App = kubernetes_deployment.getip.spec.0.template.0.metadata[0].labels.App
    }
    port {
      node_port   = 30201
      port        = 80
      target_port = 7000
    }

    type = "NodePort"
  }
}
