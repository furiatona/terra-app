package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func getIP(w http.ResponseWriter, r *http.Request) {
	url := "https://api.ipify.org?format=text"
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, "IP address: %s\n", ip)
}

func handleRequests() {
	http.HandleFunc("/", getIP)
	log.Fatal(http.ListenAndServe(":7000", nil))
}

func main() {
	handleRequests()
}
